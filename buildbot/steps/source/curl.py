
import urllib

from twisted.internet import defer
from twisted.internet import reactor
from twisted.python import log

from buildbot.interfaces import WorkerSetupError
from buildbot.process import buildstep
from buildbot.process import remotecommand
from buildbot.process import results
from buildbot.steps.source.base import Source


class Curl(Source):

    name = "curl"
    renderables = ["baseUrl", "suffix", "proxyUrl"]

    def __init__(self, baseUrl=None, branch=None, mode="tar",
                 suffix=None, proxyUrl=None, **kwargs):

        self.baseUrl = baseUrl
        self.branch = branch
        self.mode = mode
        self.suffix = suffix
        self.proxyUrl = proxyUrl

        if not self._hasAttrGroupMember('mode', self.mode):
            raise ValueError("mode {} is not one of {}".format(
                    self.mode,
                    self._listAttrGroupMembers('mode'),
                ))

        super().__init__(**kwargs)

    @defer.inlineCallbacks
    def run_vc(self, branch, revision, patch):
        self.branch = branch
        self.revision = revision

        self.filename = "{}-{}.{}".format(self.branch,
                                          self.revision,
                                          self.suffix)

        self.stdio_log = yield self.addLogForRemoteCommands("stdio")

        installed = yield self.checkCurl()
        if not installed:
            raise WorkerSetupError("curl is not installed on worker")

        installed = yield self.checkDecompression()
        if not installed:
            raise WorkerSetupError("decompression tool is not installed")

        patched = yield self.sourcedirIsPatched()
        if patched:
            pass  # clean

        yield self._getAttrGroupMember('mode', self.mode)()

        if patch:
            yield self.patch(patch)

        yield self.parseGotRevision()
        # TODO set more source property
        return results.SUCCESS

    @defer.inlineCallbacks
    def mode_tar(self):
        yield self.doClobber()

        rc = yield self.doDownload()
        if rc != 0:
            log.msg("Download failed. Abort")
            raise buildstep.BuildStepFailed()

        rc = yield self.doTarExtract()
        if rc != 0:
            log.msg("Tar extract failed. Abort")
            raise buildstep.BuildStepFailed()
        yield self.runRmFile(self.filename)

    @defer.inlineCallbacks
    def doTarExtract(self):
        """Extract source files from the release tarball"""
        command = ["tar", "-x",
                   "-f", self.filename,
                   "-C", self.workdir,
                   "--strip-components", "1"]
        rc = yield self._dovccmd(command, "", abandonOnFailure=True)
        return rc

    @defer.inlineCallbacks
    def doDownload(self):
        """Download the release tarball"""
        fullUrl = "{}/{}".format(self.baseUrl,
                                 urllib.parse.quote(self.filename))

        command = ["curl", "-o", self.filename]
        if self.proxyUrl:
            command += ["-x", self.proxyUrl]
        command += [fullUrl]

        if self.retry:
            abandonOnFailure = (self.retry[1] <= 0)
        else:
            abandonOnFailure = True

        rc = yield self._dovccmd(command,
                                 "",
                                 abandonOnFailure=abandonOnFailure)

        if self.retry:
            if self.stopped or rc == 0:
                return rc
            delay, repeats = self.retry
            if repeats > 0:
                log.msg("Download failed, trying %d more times after %d seconds"
                        % (repeats, delay))
                self.retry = (delay, repeats - 1)
                df = defer.Deferred()
                df.addCallback(lambda _: self.doDownload())
                reactor.callLater(delay, df.callback, None)
                rc = yield df
        return rc

    @defer.inlineCallbacks
    def doClobber(self):
        """Remove the work directory and create new one"""
        rc = yield self.runRmdir(self.workdir, timeout=self.timeout)
        if rc != 0:
            raise RuntimeError("Failed to delete work directory")
        rc = yield self.runMkdir(self.workdir)
        if rc != 0:
            raise RuntimeError("Failed to create work directory")

    def parseGotRevision(self):
        # TODO parse real version?
        self.updateSourceProperty('got_revision', self.revision)

    @defer.inlineCallbacks
    def checkCurl(self):
        rc = yield self._dovccmd(["curl", "--version"])
        return rc == 0

    @defer.inlineCallbacks
    def checkDecompression(self):
        res = False
        if self.mode == "tar":
            res = yield self.checkTar()
            if self.suffix == "tar.gz":
                res &= yield self.checkGzip()
            elif self.suffix == "tar.bz2":
                res &= yield self.checkBzip2()
            elif self.suffix == "tar.xz":
                res &= yield self.checkXZ()
        return res

    @defer.inlineCallbacks
    def checkTar(self):
        rc = yield self._dovccmd(["tar", "--version"])
        return rc == 0

    @defer.inlineCallbacks
    def checkGzip(self):
        rc = yield self._dovccmd(["gzip", "--version"])
        return rc == 0

    @defer.inlineCallbacks
    def checkBzip2(self):
        rc = yield self._dovccmd(["bzip2", "--version"])
        return rc == 0

    @defer.inlineCallbacks
    def checkXZ(self):
        rc = yield self._dovccmd(["xz", "--version"])
        return rc == 0

    @defer.inlineCallbacks
    def _dovccmd(self, command, workdir=None, abandonOnFailure=True,
                 initialStdin=None):
        if workdir is None:
            workdir = self.workdir
        if not command:
            raise ValueError("No command specified")
        cmd = remotecommand.RemoteShellCommand(workdir,
                                               command,
                                               env=self.env,
                                               timeout=self.timeout,
                                               logEnviron=self.logEnviron,
                                               initialStdin=initialStdin)
        cmd.useLog(self.stdio_log, False)
        yield self.runCommand(cmd)

        if cmd.rc != 0 and abandonOnFailure:
            log.msg("Source step failed while running command {}".format(cmd))
            raise buildstep.BuildStepFailed()
        return cmd.rc

    def computeSourceRevision(self, changes):
        if not changes:
            return None
        return changes[-1].revision
