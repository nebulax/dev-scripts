#!/usr/bin/env python3

import apt


def check_dup_dependencies(cache: apt.cache.Cache):
    for pkg_name in cache.keys():
        pkg = cache[pkg_name]
        if pkg.installed is None:
            continue
        for ver in pkg.versions:
            if not ver.is_installed:
                continue
            required_deps = set()
            recommend_deps = set()
            dup_dep_pairs = []
            for dep in ver.dependencies:
                all_deps = set()
                for d in dep.or_dependencies:
                    for v in d.installed_target_versions:
                        all_deps.add(v)
                if len(all_deps) > 1:
                    dup_dep_pairs.append(all_deps)
                else:
                    required_deps |= all_deps
            for dep in ver.recommends:
                all_deps = set()
                for d in dep.or_dependencies:
                    for v in d.installed_target_versions:
                        all_deps.add(v)
                if len(all_deps) > 1:
                    dup_dep_pairs.append(all_deps)
                else:
                    recommend_deps ^= all_deps
            if not dup_dep_pairs:
                continue
            for pair in dup_dep_pairs:
                to_print = []
                for v in pair:
                    if v.priority in ['required', 'important']:
                        continue
                    if v in required_deps:
                        continue
                    if v in recommend_deps:
                        continue
                    to_print.append(v)
                if not to_print:
                    continue
                print(pkg_name, ver.version)
                for v in to_print:
                    print(' -', v)


cache = apt.Cache()

check_dup_dependencies(cache)

