#!/usr/bin/env python3

import apt


def check_obsolute_versions(cache: apt.cache.Cache):
    obsolute_pkgs = []
    for pkg_name in cache.keys():
        pkg = cache[pkg_name]
        if not pkg.installed:
            continue
        for ver in pkg.versions:
            if not ver.is_installed:
                continue
            is_obsolute = True
            for orig in ver.origins:
                if orig.origin != '':
                    is_obsolute = False
                    break
            if is_obsolute:
                obsolute_pkgs.append((pkg_name, ver.version))
    if not obsolute_pkgs:
        return
    print('Obsolute Package Versions:')
    for pkg in obsolute_pkgs:
        print(' -', pkg[0], pkg[1])


cache = apt.Cache()
check_obsolute_versions(cache)

