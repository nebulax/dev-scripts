#!/usr/bin/env python3

import apt


class ReverseDep:
    def __init__(self, name):
        self.name = name
        self.dependencies = set()
        self.recommends = set()

    def add_dependency(self, v):
        self.dependencies.add(v)

    def add_recommend(self, v):
        self.recommends.add(v)


ALL_INSTALLED = []
REVERSE_DEP_MAP = {}


def add_reverse_dependency(cur, dep):
    key = f"{dep}"
    if key not in REVERSE_DEP_MAP:
        REVERSE_DEP_MAP[key] = ReverseDep(dep)
    rdep = REVERSE_DEP_MAP[key]
    rdep.add_dependency(cur)


def add_reverse_recommend(cur, dep):
    key = f"{dep}"
    if key not in REVERSE_DEP_MAP:
        REVERSE_DEP_MAP[key] = ReverseDep(dep)
    rdep = REVERSE_DEP_MAP[key]
    rdep.add_recommend(cur)


def build_reverse_dependencies(cache: apt.cache.Cache):
    for pkg_name in cache.keys():
        pkg = cache[pkg_name]
        if pkg.installed is None:
            continue
        for ver in pkg.versions:
            if not ver.is_installed:
                continue

            ALL_INSTALLED.append(ver)

            for dep in ver.dependencies:
                all_deps = set()
                for d in dep.or_dependencies:
                    for v in d.installed_target_versions:
                        add_reverse_dependency(ver, v)

            for dep in ver.recommends:
                all_deps = set()
                for d in dep.or_dependencies:
                    for v in d.installed_target_versions:
                        add_reverse_recommend(ver, v)


cache = apt.Cache()

build_reverse_dependencies(cache)

for v in ALL_INSTALLED:
    if f"{v}" not in REVERSE_DEP_MAP:
        if v.priority not in ['required', 'important']:
            print(v)

